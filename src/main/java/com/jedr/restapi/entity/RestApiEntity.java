package com.jedr.restapi.entity;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RestApiEntity {


    private  Long id;
    private String name;
    private String detail;


}
