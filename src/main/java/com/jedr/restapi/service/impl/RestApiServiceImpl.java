package com.jedr.restapi.service.impl;

import com.jedr.restapi.entity.RestApiEntity;
import com.jedr.restapi.mapper.RestApiMapper;
import com.jedr.restapi.service.RestApiService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RestApiServiceImpl implements RestApiService {
private final RestApiMapper restApiMapper;

    public RestApiServiceImpl(RestApiMapper restApiMapper) {
        this.restApiMapper = restApiMapper;
    }

    @Override
    public String createData(RestApiEntity restApi) {
       restApiMapper.saveData(restApi);
        return "Done";
    }

    @Override
    public String updateData(RestApiEntity restApi) {
        restApiMapper.updateData(restApi);
        return "Updated";
    }

    @Override
    public String deleteData(Long id) {
restApiMapper.deleteData(id);
        return null;
    }

    @Override
    public List<RestApiEntity> getAllData() {
       return restApiMapper.getAllData();
    }

    @Override
    public RestApiEntity getDataById(Long id) {
        return (RestApiEntity) restApiMapper.findById(id).get();
    }
}
