package com.jedr.restapi.mapper;

import com.jedr.restapi.entity.RestApiEntity;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.core.annotation.Order;

import java.util.List;
import java.util.Optional;

@Mapper
public interface RestApiMapper {

    List<RestApiEntity> getAllData();
    Optional<Order> findById(Long id);

    int saveData(RestApiEntity param);
    int updateData(RestApiEntity param);

    int deleteData(Long id);
}
